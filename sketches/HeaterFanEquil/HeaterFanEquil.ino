// started with from Demo code for Grove - Temperature Sensor V1.1/1.2
// from https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/


#include <math.h>
#include <Wire.h>
#include "rgb_lcd.h"


rgb_lcd lcd;
const int R0 = 100000;            // R0 = 100k
const int pinPotent = A2;     //Grove - potentiometer sensor
const int pinControlFan = 11;
const int pinTach = 2;

const float maxAnalog = 1023.0;
const float zero_inK = 273.15;      // zero degrees in kelvin
const float room_temp_inK = 298.15;  // room temp 25 degrees in kelvin

const int pinBreadTemp = A1;     // connected to breadboard readout
const float B_bread = 5445.14;  //flipped sign

const unsigned long timeBetween_change = 4*60*1000UL;
const int levels[] = {255,195,135,75,60,45,20,0};
int levelCount = sizeof(levels)/sizeof(levels[0]);

const unsigned long flip_all = 3000;
const unsigned long spots = 3;

void displayValuePrio(char* text,float temp, int row) {
  // convert float to string
  char valString[10];  // 6 + 2 + 1(dot)
  dtostrf(temp, 6, 2, valString); 
  lcd.setCursor(0, row%2);
  lcd.print(text);
  lcd.print(":");
  lcd.print(valString); 
  for (int i = strlen(text)+strlen(valString); i < 16; i++) {
    lcd.print(" ");
  }
}
void displayValue(char* text,float temp, int row) {
  unsigned long time = micros()/1000;
  //guard clause allows to use lde for more than 2 rows by 
  //essentially showing a pair each at different times
  //like 0,1 then 2,3 then 4,5 .. switching after period of flip_all
  //spots*2 is the number available rows
  if((time % (flip_all*spots))/flip_all != row/2   ) {
    return;
  } 
  displayValuePrio(text,temp,row);
}

template<typename T>
void serialPrint(T arg) {
    Serial.println(arg);
}

template<typename T, typename... Args>
void serialPrint(T first, Args... args) {
    Serial.print(first);
    Serial.print(';');
    serialPrint(args...);
}


float get_temp_bread() {
  int readoff = analogRead(pinBreadTemp);
  float R_rel = maxAnalog/readoff-1.0;
  return 1.0/(log(R_rel)/B_bread+1/room_temp_inK)-zero_inK; // convert to temperature via datasheet
}



volatile unsigned long pulseCount = 0;
volatile unsigned long pulse_last_time=0;
void countPulse() {
  pulseCount++;
  pulse_last_time = micros();
}

unsigned long last_time = 0;
unsigned long last_count;
float get_RPM() {
  unsigned long time = micros();
  float val = (pulseCount-last_count)*60.0/2/(time-last_time)*1000*1000;
  last_time = time;
  last_count = pulseCount;
  return val;
}

void setup()
{
    Serial.begin(9600);
    pinMode(pinControlFan,OUTPUT);
    pinMode(pinTach, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(pinTach) , countPulse, RISING);

    analogWrite(pinControlFan, 0);
    lcd.begin(16, 2);
    lcd.setRGB(0, 255, 255);
    serialPrint("time", "temp","readout bread","level","fancount","lastcount","rpm");
}
int index =-1;
unsigned long lastchange =0;
bool stop=false;
void loop()
{   
    unsigned long time=micros()/1000;
    if(stop) {

    } else if((time-lastchange)>timeBetween_change || index==-1) {
      index +=1;
      lastchange = time;
      if(index>=levelCount) {
          stop=true;
          return;
      }
      analogWrite(pinControlFan, levels[index]);
    }
    float temp = get_temp_bread();
    float rpm = get_RPM();

    displayValue("temp", temp,0);
    displayValue("rpm", rpm,1);

    displayValue("level", levels[index],2);
    displayValue("current", (time-lastchange)/1000.0,3);

    float secondsleft = (levelCount-index-1)*timeBetween_change/1000 +(timeBetween_change-(time-lastchange))/1000 ;
    displayValue("-time s", secondsleft,4);
    displayValue("min left", secondsleft/60,5);

    serialPrint(micros()/1000,temp,analogRead(pinBreadTemp),levels[index],pulseCount, pulse_last_time/1000,rpm);
    delay(500);
}