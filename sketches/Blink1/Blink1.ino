bool test= true;
void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
}
//makes the buildin led blink but also by connecting
//ground - led - 220 resistor - pin 13
//makes a led on the breadboard blink
void loop() {
  test= !test;
  Serial.println(test);
  delay(1000);
  digitalWrite(LED_BUILTIN, (test ? HIGH:LOW));
}