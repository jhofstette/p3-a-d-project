


int ledState=0;

template<typename T>
void serialPrint(T arg) {
    Serial.println(arg);
}

template<typename T, typename... Args>
void serialPrint(T first, Args... args) {
    Serial.print(first);
    Serial.print(';');
    serialPrint(args...);
}

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,ledState);
  serialPrint("time","ledState", "mod 200", "*2 mod 75","test");
}
float current_test =0;


void loop() {
  if (Serial.available() > 0) {
    // Read the incoming command
    String command = Serial.readStringUntil('\n');

    // Check if command is "TOGGLE"
    if (command.equals("Toggle")) {
      // Toggle LED state
      ledState = !ledState;

      // Update LED
      digitalWrite(LED_BUILTIN, ledState);

    } else if(command.startsWith("TEST")) {
      command.remove(0, 4); // Remove "TEST"
      current_test  = command.toFloat();
    }
  }
  unsigned long seconds = micros()/1000/100;
  serialPrint(micros()/1000,ledState, seconds % 200, (seconds *2)% 75,current_test);
  delay(200);
}
