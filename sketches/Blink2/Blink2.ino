#include <limits.h>
#define SHORT_LED 8

unsigned long last=0;
//delay on/off in miliseconds
unsigned long delay_long = 2000;
unsigned long delay_short = 700;
unsigned int recheck = 25;

void handle_blinking(uint8_t pin, unsigned long delay , unsigned long time) {
  //converting from miliseconds to microseconds
  delay *= 1000;
  digitalWrite(pin, ( (time % (delay*2) <delay )? HIGH:LOW));
}

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(SHORT_LED, OUTPUT);
  digitalWrite(LED_BUILTIN,HIGH);
  digitalWrite(SHORT_LED,HIGH);
}

void loop() {
  unsigned long current = micros();
  //(ULONG_MAX- last) is the carryover which only gets added when a overflow happend
  unsigned long time = (current<last)*(ULONG_MAX- last) + current;

  //multiple led could be added here with different delays
  //doesnt account for the time the code spend after the micros call
  handle_blinking(LED_BUILTIN, delay_long, time);
  handle_blinking(SHORT_LED, delay_short, time);
  last = current;
  delay(recheck);
}