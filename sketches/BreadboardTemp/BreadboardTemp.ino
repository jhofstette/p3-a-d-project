// started with from Demo code for Grove - Temperature Sensor V1.1/1.2
// from https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/


#include <math.h>
#include <Wire.h>
#include "rgb_lcd.h"


rgb_lcd lcd;

const float B = 4275;              // B value of the thermistor
const int R0 = 100000;            // R0 = 100k
const int pinTempSensor = A0;     // Grove - Temperature Sensor connect to A0

const float maxAnalog = 1023.0;
const float zero_inK = 273.15;      // zero degrees in kelvin
const float room_temp_inK = 298.15;  // room temp 25 degrees in kelvin

const int pinBreadTemp = A1;     // connected to breadboard readout
const float B_bread = 6286.77;  //flipped sign

const unsigned long flip_all = 3000;
const unsigned long spots = 2;

void displayValue(char* text,float temp, int row) {
  unsigned long time = micros()/1000;
  //guard clause allows to use lde for more than 2 rows by 
  //essentially showing a pair each at different times
  //like 0,1 then 2,3 then 4,5 .. switching after period of flip_all
  //spots*2 is the number available rows
  if((time % (flip_all*spots))/flip_all != row/2   ) {
    return;
  } 

  // convert float to string
  char valString[10];  // 6 + 2 + 1(dot)
  dtostrf(temp, 6, 2, valString); 
  lcd.setCursor(0, row%2);
  lcd.print(text);
  lcd.print(":");
  lcd.print(valString); 
  for (int i = strlen(text)+strlen(valString); i < 16; i++) {
    lcd.print(" ");
  }
}

template<typename T>
void serialPrint(T arg) {
    Serial.println(arg);
}

template<typename T, typename... Args>
void serialPrint(T first, Args... args) {
    Serial.print(first);
    Serial.print(';');
    serialPrint(args...);
}

float get_temp_grove() {
  int readoff = analogRead(pinTempSensor);
  float R_rel = maxAnalog/readoff-1.0;

  return 1.0/(log(R_rel)/B+1/room_temp_inK)-zero_inK; // convert to temperature via datasheet
}

float get_temp_bread() {
  int readoff = analogRead(pinBreadTemp);
  float R_rel = maxAnalog/readoff-1.0;

  return 1.0/(log(R_rel)/B_bread+1/room_temp_inK)-zero_inK; // convert to temperature via datasheet
}

float expected_B(int readoff, float cels_temp) {
  float R_rel = maxAnalog/readoff-1.0;
  float K_temp = cels_temp+zero_inK;
  return log(R_rel)*room_temp_inK*K_temp/(K_temp - room_temp_inK);
}
void setup()
{
    Serial.begin(9600);
    lcd.begin(16, 2);
    lcd.setRGB(0, 255, 255);
    serialPrint("time", "temp grove", "temp bread", "readout grove", "readout bread","expected B");
}
void loop()
{
    float temperature = get_temp_grove();
    float temp_bread = get_temp_bread();

    int readoff = analogRead(pinBreadTemp);
    float expe_B = expected_B(readoff,temperature);
    serialPrint(micros()/1000,temperature,temp_bread,analogRead(pinTempSensor)    ,readoff, expe_B );
    displayValue("grove", temperature,0);
    displayValue("bread", temp_bread,1);
    displayValue("b val", expe_B,2);

    displayValue("bread", temp_bread,3);
    delay(200);
}