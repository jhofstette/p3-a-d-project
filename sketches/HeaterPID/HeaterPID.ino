// started with from Demo code for Grove - Temperature Sensor V1.1/1.2
// from https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/

#include <limits.h>
#include <math.h>
#include <Wire.h>
#include "rgb_lcd.h"


rgb_lcd lcd;
const int R0 = 100000;            // R0 = 100k
const int pinButton = A0;    
const int pinPotent = A2;     //Grove - potentiometer sensor
const int pinControlFan = 11;
const int pinTach = 2;

const float maxAnalog = 1023.0;
const float zero_inK = 273.15;      // zero degrees in kelvin
const float room_temp_inK = 298.15;  // room temp 25 degrees in kelvin

const int pinBreadTemp = A1;     // connected to breadboard readout
const float B_bread = 5445.14;  //flipped sign

//lower and upper bound for control temp
const float high_end = 46.0;
const float low_end = 20.0;


const unsigned long flip_all = 3000;
const unsigned long spots = 2;

void displayValuePrio(char* text,float temp, int row) {
  // convert float to string
  char valString[10];  // 6 + 2 + 1(dot)
  dtostrf(temp, 6, 2, valString); 
  lcd.setCursor(0, row%2);
  lcd.print(text);
  lcd.print(":");
  lcd.print(valString); 
  for (int i = strlen(text)+strlen(valString); i < 16; i++) {
    lcd.print(" ");
  }
}
void displayValue(char* text,float temp, int row) {
  unsigned long time = micros()/1000;
  //guard clause allows to use lde for more than 2 rows by 
  //essentially showing a pair each at different times
  //like 0,1 then 2,3 then 4,5 .. switching after period of flip_all
  //spots*2 is the number available rows
  if((time % (flip_all*spots))/flip_all != row/2   ) {
    return;
  } 
  displayValuePrio(text,temp,row);
}

template<typename T>
void serialPrint(T arg) {
    Serial.println(arg);
}

template<typename T, typename... Args>
void serialPrint(T first, Args... args) {
    Serial.print(first);
    Serial.print(';');
    serialPrint(args...);
}


float get_temp_bread() {
  int readoff = analogRead(pinBreadTemp);
  float R_rel = maxAnalog/readoff-1.0;
  return 1.0/(log(R_rel)/B_bread+1/room_temp_inK)-zero_inK; // convert to temperature via datasheet
}

bool use_K_direct = true;
int method = 0;
float period = 1;
float K_p_main = 1;

float K_p_control = 33;
float K_i_control = 3.18/2;
float K_d_control = 85.49*1.5;

float K_p = 1;
float K_i = 0;
float K_d = 0;
//lower and upper bound for the integral
const float integ_upper = 255;
const float integ_lower =-255;
const float max_change = 200; //max change in seconds

float prev_error = 0;
float integral = 0;
float last_output=0;

unsigned long min_delay = 20;
unsigned long last_check = 0;

bool limited_change =false;

float pidController(float setpoint, float processpoint) {
    unsigned long current_micro = micros();
    //handling overflow
    unsigned long time = current_micro;
    if(current_micro<last_check) {
      time+= (ULONG_MAX- last_check);
      last_check =0;
    }
    unsigned long delay = time-last_check ;
    float delay_s = delay/1000.0/1000 ;
    float error = setpoint - processpoint;
    float newIntegral = integral + K_i*delay_s*error;
    if(newIntegral>integ_upper) newIntegral = integ_upper;
    if(newIntegral<integ_lower) newIntegral = integ_lower;
    float deriv = K_d*(error-prev_error)/delay_s;
    float output = K_p*error+ newIntegral + deriv;
    float max_change_current = max_change*(delay_s);
    limited_change = abs(output)>max_change_current;
    output = constrain(output,-max_change_current,max_change_current);
    if(delay>min_delay) {
      last_check = current_micro;
      integral = newIntegral;
      prev_error = error;
      last_output= output;
    }
    return output; 
}


volatile unsigned long pulseCount = 0;
volatile unsigned long pulse_last_time=0;
void countPulse() {
  pulseCount++;
  pulse_last_time = micros();
}

unsigned long last_time = 0;
unsigned long last_count = 0;
float get_RPM() {
  unsigned long time = micros();
  float val = (pulseCount-last_count)*60.0/2/(time-last_time)*1000*1000;
  last_time = time;
  last_count = pulseCount;
  return val;
}
float last_level=0;
void setup()
{
    Serial.begin(9600);
    pinMode(pinButton, INPUT);
    pinMode(pinControlFan,OUTPUT);
    pinMode(pinTach, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(pinTach) , countPulse, RISING);

    lcd.begin(16, 2);
    lcd.setRGB(0, 255, 255);
    float temp = get_temp_bread();
    float target = (high_end-low_end) *analogRead(pinPotent)/maxAnalog + low_end;
    last_level = temp>target ? 255: 0;
    analogWrite(pinControlFan, last_level);
    serialPrint("time", "temp","target","level","fancount","rpm","limited_change", "period", "K_p", "K_i", "K_d","integral");
    use_correct_method();
}

void use_direct() {
  K_p = K_p_control;
  K_i = K_i_control;
  K_d = K_d_control;
}
void use_method_p() {
    K_p = K_p_main*0.5;
    K_i = 0;
    K_d = 0;
}
void use_method_pi() {
    if(period ==0) {
      period = 1;
    }
    K_p = K_p_main*0.45;
    K_i = K_p_main*0.54/period;
    K_d = 0;
}
void use_method_pid() {
    if(period ==0) {
      period = 1;
    }
    K_p = K_p_main*0.60;
    K_i = K_p_main*1.2/period;
    K_d = K_p_main*3*period/40;
}
void use_correct_method() {
  if (use_K_direct) {
    use_direct();
  } else {
    switch (method) {
      case 1:
        use_method_pi();
        break;
      case 2: 
        use_method_pid();
        break;
      default:
        use_method_p();
    }
  }
}
void handle_serial_input() {
  if (Serial.available() > 0) { 

      // Read the incoming command
      String command = Serial.readStringUntil('\n');
      String read_input_arr[] = {"Period", "K_p", "K_i", "K_d"};
      // Check if command is "TOGGLE"
      if (command.equals("Toggle_Direct")) {
        use_K_direct = !use_K_direct;
        //when switching from direct to method, set main to the value control for K_p
        if (!use_K_direct) {
          K_p_main = K_p_control;
        }

      } else if (command.equals("Toggle_Method")) {
        method = (method+1)%3;

      } else if (command.equals("Zero_Integral")) {
        integral = 0;

      }else if(command.startsWith(read_input_arr[0])) {
        command.remove(0, read_input_arr[0].length()); 
        period  = command.toFloat();
      } else if(command.startsWith(read_input_arr[1])) {
        command.remove(0, read_input_arr[1].length()); 
        if (use_K_direct) {
          K_p_control  = command.toFloat();
        } else {
          K_p_main  = command.toFloat();
        }
      } else if(command.startsWith(read_input_arr[2])) {
        command.remove(0, read_input_arr[2].length()); 
        K_i_control  = command.toFloat();
      } else if(command.startsWith(read_input_arr[3])) {
        command.remove(0, read_input_arr[3].length()); 
        K_d_control  = command.toFloat();
      }

      use_correct_method();


  }
}
void loop()
{   

    handle_serial_input();
    float temp = get_temp_bread();
    float target = (high_end-low_end) *analogRead(pinPotent)/maxAnalog + low_end;
    float rpm = get_RPM();
    float pidResponse = pidController(temp,target);
    float level = constrain(last_level + pidResponse,0,255);
    displayValue("temp", temp,0);
    displayValue("target", target,1);
    displayValue("level", level,2);
    displayValue("rpm", rpm,3);

    analogWrite(pinControlFan, int(level));

    serialPrint(micros()/1000,temp,target,level,pulseCount,rpm,limited_change, period, K_p, K_i, K_d,integral);

    last_level = level;
    delay(200);
}