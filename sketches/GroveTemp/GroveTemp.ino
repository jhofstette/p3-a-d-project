// started with from Demo code for Grove - Temperature Sensor V1.1/1.2
// from https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/


#include <math.h>
#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

const float B = (4299.0 + 4250.0)/2;               // B value of the thermistor
const int R0 = 100000;            // R0 = 100k
const int pinTempSensor = A0;     // Grove - Temperature Sensor connect to A0

const float maxAnalog = 1023.0;
const float zero_inK = 273.15;      // zero degrees in kelvin
const float room_temp_inK = 298.15;  // room temp 25 degrees in kelvin
const int pinPotSensor = A2;     //Grove - potentiometer sensor
const float tempLowBound = 15.0;
const float tempHighBound = 40.0;


void displayTemp(float temp) {
  // convert float to string
  char valString[6];  // 3 + 2 + 1(dot)
  dtostrf(temp, 3, 2, valString); 
  lcd.setCursor(0, 1);
  lcd.print("temp:   ");
  lcd.print(valString);
}
void displayCutoff(float cutoff) {
  // convert float to string
  char valString[5];  // 2 + 2 + 1(dot)
  dtostrf(cutoff, 2, 2, valString); 
  lcd.setCursor(0, 0);
  lcd.print("cutoff: ");
  lcd.print(valString);
}

float get_temp(int readoff) {
  float R = maxAnalog/readoff-1.0;
  return 1.0/(log(R)/B+1/room_temp_inK)-zero_inK; // convert to temperature via datasheet
}
void setup()
{
    Serial.begin(9600);
    lcd.begin(16, 2);
    lcd.setRGB(0, 255, 255);
}
void loop()
{
    int a = analogRead(pinTempSensor);
    float temperature = get_temp(a);
    int b= analogRead(pinPotSensor);
    float cutoff= (tempHighBound- tempLowBound)*b/maxAnalog + tempLowBound;
    displayTemp( temperature);
    displayCutoff(cutoff);
    if(cutoff>=temperature) {
      lcd.setRGB(0, 255, 255);
    } else {
      lcd.setRGB(255,0,0);
    }

    delay(200);
}