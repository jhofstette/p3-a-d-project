// started with from Demo code for Grove - Temperature Sensor V1.1/1.2
// from https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/


#include <math.h>
#include <Wire.h>
#include "rgb_lcd.h"


rgb_lcd lcd;
const int R0 = 100000;            // R0 = 100k
const int pinPotent = A2;     //Grove - potentiometer sensor
const int pinControlFan = 11;
const int pinTach = 2;
const int pinButton = A0;

const float maxAnalog = 1023.0;
const float zero_inK = 273.15;      // zero degrees in kelvin
const float room_temp_inK = 298.15;  // room temp 25 degrees in kelvin

const int pinBreadTemp = A1;     // connected to breadboard readout
const float B_bread = 5445.14;  //flipped sign

//2 point constants
//lower and upper bound for the high end
const float high_hend = 46.0;
const float low_hend = 20.0;
//max difference between the low and high temp of the 2 point control
const float boundary_spacing = 1.5;
const int maxSetting=30;

const unsigned long flip_all = 3000;
const unsigned long spots = 1;

void displayValuePrio(char* text,float temp, int row) {
  // convert float to string
  char valString[10];  // 6 + 2 + 1(dot)
  dtostrf(temp, 6, 2, valString); 
  lcd.setCursor(0, row%2);
  lcd.print(text);
  lcd.print(":");
  lcd.print(valString); 
  for (int i = strlen(text)+strlen(valString); i < 16; i++) {
    lcd.print(" ");
  }
}
void displayValue(char* text,float temp, int row) {
  unsigned long time = micros()/1000;
  //guard clause allows to use lde for more than 2 rows by 
  //essentially showing a pair each at different times
  //like 0,1 then 2,3 then 4,5 .. switching after period of flip_all
  //spots*2 is the number available rows
  if((time % (flip_all*spots))/flip_all != row/2   ) {
    return;
  } 
  displayValuePrio(text,temp,row);
}

template<typename T>
void serialPrint(T arg) {
    Serial.println(arg);
}

template<typename T, typename... Args>
void serialPrint(T first, Args... args) {
    Serial.print(first);
    Serial.print(';');
    serialPrint(args...);
}


float get_temp_bread() {
  int readoff = analogRead(pinBreadTemp);
  float R_rel = maxAnalog/readoff-1.0;
  return 1.0/(log(R_rel)/B_bread+1/room_temp_inK)-zero_inK; // convert to temperature via datasheet
}

int last_read = -1;
//press button and set difference between low/high points via potentiometer
//dont press button to then control high point via potentiometer
bool set_low_high_temp(float& low, float & high) {
    int readPot = analogRead(pinPotent);
    bool changed = last_read != readPot;
    last_read =readPot;
    high = (high_hend-low_hend) *readPot/maxAnalog + low_hend;
    low = high- boundary_spacing;
    return changed;

}


volatile unsigned long pulseCount = 0;
volatile unsigned long pulse_last_time=0;
void countPulse() {
  pulseCount++;
  pulse_last_time = micros();
}

unsigned long last_time = 0;
unsigned long last_count =0;
float get_RPM() {
  unsigned long time = micros();
  float val = (pulseCount-last_count)*60.0/2/(time-last_time)*1000*1000;
  last_time = time;
  last_count = pulseCount;
  return val;
}

bool cooling= false;
void setup()
{
    Serial.begin(9600);
    pinMode(pinButton, INPUT);
    pinMode(pinControlFan,OUTPUT);
    pinMode(pinTach, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(pinTach) , countPulse, RISING);

    analogWrite(pinControlFan, 0);
    float temp = get_temp_bread();
    float low =0;
    float high = 0;
    //set via button and potentiometer
    bool changed = set_low_high_temp(low,high);
    cooling = temp<high;
    lcd.begin(16, 2);
    lcd.setRGB(0, 255, 255);
    serialPrint("time", "temp","readout bread","low temp","high temp","iscooling","fancount","lastcount","rpm","forcefan");
}
void loop()
{
    float temp = get_temp_bread();
    float low =0;
    float high = 0;
    float rpm = get_RPM();

    //set via button and potentiometer
    bool changed = set_low_high_temp(low,high);
    //displayValue("button", digitalRead(pinButton),0);
    //displayValue("bread", temp_bread,1);
    displayValue("low", low,2);
    displayValue("high", high,3);
    displayValue("temp", temp,0);
    displayValue("rpm", rpm,1);
    if((cooling && temp<low) || (!cooling && temp>high)) {
      cooling = !cooling;
    }
    analogWrite(pinControlFan, cooling ? maxSetting: 0);

    bool forcefan= digitalRead(pinButton);
    if(forcefan) {
      analogWrite(pinControlFan, 255);
    }

    if(changed) {
      displayValuePrio("low", low,0);
      displayValuePrio("high", high,1);
      
    }
    serialPrint(micros()/1000,temp,analogRead(pinBreadTemp),low ,high,cooling,pulseCount, pulse_last_time/1000,rpm,forcefan);
    delay(500);
}