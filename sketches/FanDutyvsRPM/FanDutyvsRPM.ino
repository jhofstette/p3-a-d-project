// started with from Demo code for Grove - Temperature Sensor V1.1/1.2
// from https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/


#include <math.h>
#include <Wire.h>
#include "rgb_lcd.h"


rgb_lcd lcd;

const int pinControlFan = 11;
const int pinTach = 2;

const float maxAnalog = 1023.0;

const int delay_start = 25*1000;
const int timeBetween_change = 4000;

const unsigned long flip_all = 2000;
const unsigned long spots = 2;

void displayValuePrio(char* text,float temp, int row) {
  // convert float to string
  char valString[10];  // 6 + 2 + 1(dot)
  dtostrf(temp, 6, 2, valString); 
  lcd.setCursor(0, row%2);
  lcd.print(text);
  lcd.print(":");
  lcd.print(valString); 
  for (int i = strlen(text)+strlen(valString); i < 16; i++) {
    lcd.print(" ");
  }
}
void displayValue(char* text,float temp, int row) {
  unsigned long time = micros()/1000;
  //guard clause allows to use lde for more than 2 rows by 
  //essentially showing a pair each at different times
  //like 0,1 then 2,3 then 4,5 .. switching after period of flip_all
  //spots*2 is the number available rows
  if((time % (flip_all*spots))/flip_all != row/2   ) {
    return;
  } 
  displayValuePrio(text,temp,row);
}

template<typename T>
void serialPrint(T arg) {
    Serial.println(arg);
}

template<typename T, typename... Args>
void serialPrint(T first, Args... args) {
    Serial.print(first);
    Serial.print(';');
    serialPrint(args...);
}



volatile unsigned long pulseCount = 0;
volatile unsigned long pulse_last_time=0;
void countPulse() {
  pulseCount++;
  pulse_last_time = micros();
}

unsigned long last_time = 0;
unsigned long last_count;
float get_RPM() {
  unsigned long time = micros();
  float val = (pulseCount-last_count)*60.0/2/(time-last_time)*1000*1000;
  last_time = time;
  last_count = pulseCount;
  return val;
}

void setup()
{
    Serial.begin(9600);
    pinMode(pinControlFan,OUTPUT);
    pinMode(pinTach, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(pinTach) , countPulse, RISING);

    analogWrite(pinControlFan, 0);
    lcd.begin(16, 2);
    lcd.setRGB(0, 255, 255);
    serialPrint("time", "level", "pulsecount","rpm");
    delay(delay_start);
}
int level =0;
int add = 1;
int levelcount=0;
unsigned long lastchange =0;

void loop()
{   
    unsigned long time=micros()/1000;
    if((time-lastchange)>timeBetween_change ) {
      if(level==0) {
        add= 1;
      }
      if(level==255) {
        add=-1;
      }
      level+=add;
      levelcount+=1;
      lastchange = time;
      analogWrite(pinControlFan, level);
    }

    float rpm = get_RPM();

    displayValue("level", level,0);
    displayValue("rpm", rpm,1);
    float secondsleft = (510-levelcount)*timeBetween_change/1000;
    displayValue("-time s", secondsleft,2);
    displayValue("min left", secondsleft/60,3);
    serialPrint(time,level,pulseCount,rpm);
    delay(100);
}