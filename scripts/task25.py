import matplotlib.pyplot as plt
import numpy as np
from utils.csv_handling import *
from utils.numeric_math import *

B_bread = 5445.14
data = get_data_fromsearchkey("pid")
time = data["time"]*1e-3
temp = data["temp"]
target = data["target"]
plt.plot(time,temp)
plt.plot(time,target)

plt.xlabel("time (s)")
plt.ylabel("temp (Celisus)")
#save_current_plot("task24 steps")
plt.show()
