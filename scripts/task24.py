import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from utils.csv_handling import *
from utils.numeric_math import *

B_bread = 5445.14
data = get_data_fromsearchkey("task24")
time = data["time"]*1e-3
levels = data["level"]
temp = data["temp"]
plt.plot(time,temp)
plt.title("task 24 steps with level : 255,195,135,75,60,45,20,0")
plt.xlabel("time (s)")
plt.ylabel("temp (Celisus)")
save_current_plot("task24 steps")
plt.show()


unique_a = np.unique(levels)
equil_temp= list()
# Perform linear fitting for each unique value of a
for level in unique_a:
    # Get indices where a equals the current value
    indices = np.where(levels == level)[0]
    
    # Slice b corresponding to the current value of a
    sliced_temp = temp[indices]
    sliced_temp = sliced_temp[len(sliced_temp)//2:]
    indices = indices[len(sliced_temp)//2:]
    
    # Linear fit using curve_fit from scipy.optimize
    def constant_model(x, c):
        return c

    popt, _ = curve_fit(constant_model, indices, sliced_temp)
    equil_temp.append(popt[0])

plt.scatter(unique_a,equil_temp)
plt.title("for each level take the later half of data and constant fit to get equil temp")
plt.xlabel("level duty cycle")
plt.ylabel("equil temp (Celisus)")
save_current_plot("task24-equil_temp")
plt.show()

