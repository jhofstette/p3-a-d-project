import numpy as np
zero_inK = 273.15    # zero degrees in kelvin
room_temp_inK = 298.15  # room temp 25 degrees in kelvin
maxAnalog = 1023


def getThermistorTemp(readoff,B) :
    """
    returns temperature in kelvin
    """
    R_rel = maxAnalog/readoff-1.0
    return 1.0/(np.log(R_rel)/B+1/room_temp_inK)

def expected_B(readoff,Kelv_temp) :
  R_rel = maxAnalog/readoff-1.0
  return np.log(R_rel)*room_temp_inK*Kelv_temp/(Kelv_temp - room_temp_inK)