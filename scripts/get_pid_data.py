from utils.serial_read_plot import *
from utils.csv_handling import *


def get_set_data(index):
    data = get_data_fromsearchkey(f"proper set {index}")

    y_list=[[1,2],[3],[11]]
    y_axis=["temp","level","integral" ]
    y_names = [["temp","target"],["rpm"],["integral"]]
    number_shown = 5000

    extrema_settings= [PlotExtremaPeriodSetting(1,0,[8,6,4,2,1])]
    derivative_settings = [
        PlotDerivSetting(4,"pulsecount max 255",1,show_zero=False,w_deriv = 10, w_avg=10, max = 255)
    ]
    title = f"K_p : {data['K_p'][0]}, K_i : {data['K_i'][0]}, K_d : {data['K_d'][0]}"

    plot_data(data,x_index = 0, x_axis="time (s)",y_list= y_list,y_axis= y_axis,main_title=title,
            number_shown=number_shown, y_names=y_names,x_scaling = 1e-03,
            extrema_settings = extrema_settings, derivative_settings=derivative_settings )
    save_current_plot(f"pid data proper set {index}")
    plt.show()
for i in range(1,6):
    get_set_data(i)
