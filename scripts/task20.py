import matplotlib.pyplot as plt
import numpy as np
from utils.csv_handling import *
from utils.numeric_math import *

def plot_from_keyword(keyword,spacing,duty):
    data = get_data_fromsearchkey(keyword)
    time = data["time"]*1e-3
    line = plt.plot(time,data["temp"],label=f"spacing {spacing}, duty: {duty}")
    lower_bound = data["low_temp"]
    plt.plot(time,lower_bound,color = line[0].get_color())

plot_from_keyword("duty120", 2,120)
plot_from_keyword("duty60", 2,60)
plot_from_keyword("duty30", 1.5,30)

plt.xlabel("time (s)")
plt.ylabel("temp (Celisus)")
plt.legend()
plt.show()

plot_from_keyword("duty120", 2,120)
plot_from_keyword("spacing3", 3,255)
plot_from_keyword("spacing1-8", 1.8,255)

plt.xlabel("time (s)")
plt.ylabel("temp (Celisus)")
plt.legend()
plt.show()