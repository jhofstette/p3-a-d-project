from utils.serial_read_plot import *

from utils.numeric_math import *

name = "pid proper set 5"
port = "COM3"
show_plot=True
number_shown = 1000
count_zerocrossing = [6,4,2,1]

def toggle_direct(ser):
    send_command(ser,"Toggle_Direct")
    print("toggled direct")

def toggle_method(ser):
    send_command(ser,"Toggle_Method")
    print("toggled method")

def zero_integral(ser):
    send_command(ser,"Zero_Integral")
    print("set integral to zero")


def set_period(ser):
    read_and_send(ser,"Period","input last period in seconds")

def set_K_prop(ser):
    read_and_send(ser,"K_p","input K_p")

def set_K_int(ser):
    read_and_send(ser,"K_i","input K_i")

def set_K_diff(ser):
    read_and_send(ser,"K_d","input K_d")

extrema_settings= [PlotExtremaPeriodSetting(1,0,count_zerocrossing)]
derivative_settings = [
    #PlotDerivSetting(1,"temp measured",0,w_deriv = 10, w_avg=10,scale = 3, offset = 36),
    PlotDerivSetting(4,"pulsecount max 255",1,show_zero=False,w_deriv = 10, w_avg=10, max = 255)
]
call_dict = {"t":toggle_direct,"m":toggle_method, "o":set_period,
                "p": set_K_prop, "i":set_K_int, "d" : set_K_diff,
                "0":zero_integral}

reset_read_serial(name, port,call_dict=call_dict, plot_all=0.4,show_plot = show_plot,x_index=0,x_axis="time",
                    y_list=[[1,2],[3],[8],[9],[10],[11]],
                    y_axis=["temp","level","K_p","K_i","K_d","integral" ],
                    y_names = [["temp","target"],["rpm"],["K_p"],["K_i"],["K_d"],["integral"]],
                    number_shown = number_shown,x_scaling = 1e-03,
                    derivative_settings= derivative_settings, extrema_settings = extrema_settings   )