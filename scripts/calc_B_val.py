from lab_ad import *
from utils.numeric_math import *
from utils.csv_handling import *
from symbolic_calc import *


B_grove = -4275
rel_B_grove = -250/B_grove

B_bread = -4600
rel_B_bread = -138/B_bread

data = get_data_fromsearchkey("task14")
time = data["time"]*1e-3
read_grove_data = data["readout_grove"]
read_bread_data = data["readout_bread"]
grove_read,d_grove_read = get_equil_value(time,read_grove_data,) #fit_name="grove task14")
bread_read, d_bread_read = get_equil_value(time,read_bread_data,)# fit_name="breadboard task14")

T_f, rel_T_f = dict_np_func[T]

#parameter_list_T = [T_0,R_0,R_t,B,p] 
input_grove_T = [room_temp_inK,1e5,1e5,B_grove,grove_read]
input_grove_rel_T = [room_temp_inK,0,1e5,0.01,1e5,0.01,B_grove,rel_B_grove, grove_read,d_grove_read/grove_read]
T_grove, rel_T_grove = T_f(*input_grove_T),rel_T_f(*input_grove_rel_T)


input_bread_T = [room_temp_inK,1e5,1e5,B_bread,bread_read]
input_bread_rel_T = [room_temp_inK,0,1e5,0.01,1e5,0.01,B_bread,rel_B_bread, bread_read,d_bread_read/bread_read]
T_bread, rel_T_bread = T_f(*input_bread_T),rel_T_f(*input_bread_rel_T)

B_f, rel_B_f = dict_np_func[B]
#parameter_list_B = [T_0,R_0,R_t,T,p]
input_get_B = [room_temp_inK,1e5,1e5,T_grove,bread_read]
input_get_rel_B = [room_temp_inK,0,   1e5,0.01,   1e5,0.01,  
                   T_grove,rel_T_grove,    bread_read,d_bread_read/bread_read]
B_calc_bread,rel_B_calc_bread = B_f(*input_get_B),rel_B_f(*input_get_rel_B)




#parameter_list_T = [T_0,R_0,R_t,B,p]

print(f" grove temp: {T_grove-zero_inK} +-      {rel_T_grove*T_grove}")
print(f" bread temp: {T_bread-zero_inK} +-      {rel_T_bread*T_bread}")

print(f" calculated B bread: {B_calc_bread} +-     {B_calc_bread*rel_B_calc_bread}")





