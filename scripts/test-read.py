from utils.serial_read_plot import*

def do_something(ser):
    send_command(ser,"Toggle")


def read_data_test(ser):
    read_and_send(ser,"TEST","give number")
count_zerocrossing = [6,4,2,1]
if __name__ == "__main__":
    extrema_settings= [PlotExtremaPeriodSetting(3,0,count_zerocrossing)]
    derivative_settings = [PlotDerivSetting(3,"something",0,scale = 1, offset = 1)]
    call_dict = {"a":do_something, "r":read_data_test}

    reset_read_serial("test", "COM3",call_dict=call_dict, show_plot = True,x_index=0,x_axis="time",y_list=[[1],[2,3],[4]],y_axis=["time","time","test"],
                      y_names = [["led"],["seconds*10 mod 200","seconds*20 mod 75"],["test"]],x_scaling = 1e-03,
                      derivative_settings= derivative_settings, extrema_settings = extrema_settings   )
    