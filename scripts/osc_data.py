import matplotlib.pyplot as plt
import numpy as np
from utils.csv_handling import *
from utils.numeric_math import *

data = get_data_fromsearchkey("task23")

time = data["time"]
pulse = data["pulsecount"]
time2,rpm = num_deriv(time,pulse,10)

lvl = data["level"]

plt.plot(time,data["rpm"]*  np.max(lvl)/np.max(data["rpm"]), label="loopwise calculated rpm, scaled")
plt.plot(time2,rpm * np.max(lvl)/np.max(rpm), label="rpm, points used spaced by 10")
plt.plot(time,lvl, label="duty cycle")
plt.legend()
plt.show()

