import serial
import csv
import os
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import keyboard

import threading
import sys
import time

from utils.numeric_math import *


BAUD_RATE_DEF = 9600
FOLDER_DEF = "read_data"


def suppress_output():
    sys.stdout = open(os.devnull, 'w')

def restore_output():
    sys.stdout.close()
    sys.stdout = sys.__stdout__


class PlotExtremaPeriodSetting:
    """
    contains info used for function :func:`plot_data`
    adds info to the title about the timedifference between maxima /minima (timedifference between extrema *2)

    "data_index" : index in the dataset for which you calculate and plot the derivative
    "plot_tobe" : in which plot (index of it) it should be plotted in
    "count_extrema" : array countaining the number of extrams to be used to calculate avg period
                            (one period being the time needed for 2 extrams, one min one max)
    "round_decimal" : how many decimal places should be rounded
    "w_deriv": window for when calculating the derivative nummerically
    "w_avg" : window over which the derivative then gets sliding averaged


    """
    def __init__(self, data_index, plot_tobe, count_extrema, unit="", round_decimal=2, w_deriv=10, w_avg=10):
        self.data_index = data_index
        self.plot_tobe = plot_tobe
        self.count_extrema = count_extrema
        self.unit = unit
        self.round_decimal = round_decimal
        self.w_deriv = w_deriv
        self.w_avg = w_avg

class PlotDerivSetting:
    """
    contains info used for function :func:`plot_data`
    plots numerically calculated and smoothed derivative of a dataset

    "data_index" : index in the dataset for which you calculate and plot the derivative
    "name": name of derivative
    "plot_tobe" : in which plot (index of it) it should be plotted in
    "offset" : which offset the derivative should be plotted at
    "scale" : if not none derivative gets rescalled so that its abs max is scale
    "w_deriv": window for when calculating the derivative nummerically
    "w_avg" : window over which the derivative then gets sliding averaged

    """
    def __init__(self, data_index, name, plot_tobe,show_zero=True, offset=0, scale=0, max = 0,w_deriv=1, w_avg=1):
        self.data_index = data_index
        self.name = name
        self.plot_tobe = plot_tobe
        self.offset = offset
        self.scale = scale
        self.max = max
        self.show_zero = show_zero
        self.w_deriv = w_deriv
        self.w_avg = w_avg

def handle_normal_plot(x_data,y_data,y_names,keep_label_track,rows,cols):


    for i,y_toplot in enumerate(y_data):
        plt.subplot(rows,cols,i+1)
        for j,y in enumerate(y_toplot):
            if not y is None :
                if y_names:
                    plt.plot(x_data,y,label=y_names[i][j])
                    keep_label_track[i] +=1
                else:
                    plt.plot(x_data,y)

def handle_deriv_plot(x_data,data_arr, start_index,keep_label_track, derivative_settings,rows,cols):
    for setting in derivative_settings:
        if(len(x_data)<=setting.w_deriv+ setting.w_avg) :
            continue
        y= np.array(data_arr[setting.data_index])[start_index:].astype(float)
        x_deriv,y_deriv = num_deriv(x_data,y,w=setting.w_deriv)
        x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,w=setting.w_avg)
        y_scale = setting.scale if setting.scale!=0  else 1
        if setting.max :
            y_scale = setting.max/np.max(np.abs(y_deriv))
        plt.subplot(rows,cols,setting.plot_tobe+1)
        line = plt.plot(x_deriv,setting.offset + y_deriv*y_scale,label=f"deriv {setting.name}")
        if setting.show_zero:
            line = plt.plot(x_deriv,setting.offset + y_deriv*0,color = line[0].get_color())
        keep_label_track[setting.plot_tobe] +=1

def handle_extrema_periods(x_data,data_arr, start_index,extrema_settings,rows,cols):
    for setting in extrema_settings:
        per_accepted = []
        period_calc = []
        y= np.array(data_arr[setting.data_index])[start_index:].astype(float)
        count_extrema= setting.count_extrema
        viable,extremas = get_num_extrema(x_data,y, with_deriv=False,w_deriv= setting.w_deriv, w_avg= setting.w_avg)
        if( not viable) or (len(count_extrema)==0):
            continue
        for per_count in count_extrema:
            if len(extremas)>per_count:
                period = (extremas[-1]-extremas[-1-per_count])*2/per_count
                per_accepted.append(per_count)
                period_calc.append(round(period, setting.round_decimal))
        
        high_vs_low =""
        if len(extremas)>1:
            interp_func = interp1d(x_data, y)  
            high_vs_low = f"\n last height diff: {abs(interp_func(extremas[-1])- interp_func(extremas[-2]))}"
        title = f"for zero crossing count {per_accepted} \n got periods {period_calc} {setting.unit} {high_vs_low} "
        plt.subplot(rows,cols,setting.plot_tobe+1)
        plt.title(f"{plt.gca().get_title()} \n {title}")

#main plot function
def plot_data(data_arr, x_index,x_axis,y_list,y_axis,plots_extra = 0,main_title=None,y_title=None,number_shown=1000, delay = 0.01,animate=False,show_plot=False,
                y_names=[],x_scaling=1,y_scaling=[],derivative_settings=[], extrema_settings=[]     ):
    """
    data_arr: array of arrays of the data, first entry of the arrays gets ignored
    options: dictionary used to do the plotting
    viable/necessary keys (* marks necessary):
    *"x_index" : index in the data_arr to get the array that should be used for the x axis
    "x_axis" : name of the x axis
    *"y_list" : list of (list of index), each entry goes on a separate plot,
               if an entry is a list they get plotted on the same plot
    "y_names" : should have same format as y_list , but with names for each thing plotted
    "y_axis" : name of each plot
    "plots_extra": how many extra plots should be added (y_axis and y_names if used need to have extra values)
                    useful if derivative of some data should be shown separatly 
    "main_title": title for the entire set of plots, if None just gives the amount of datapoints shown
    "y_title" : title of each plot
    "number_shown" : how many datapoints max should be shown
    "x_scaling" : scaling of x axis
    "y_scaling" : same format as y_list, scaling of y data

    "derivative_settings" : shows numerical derivative of given index in the same plot
    "extrema_settings": gives information about spacing for extremals

    """
    #get data together and scale it
    x_data = data_arr[x_index]
    start_index = max(1,len(x_data)-number_shown)
    x_data = np.array(x_data[start_index:]).astype(float)*x_scaling

    y_data = [
        [np.array(data_arr[y_index])[start_index:].astype(float) if y_index>=0 else None  for y_index in y_blob] for y_blob in y_list
    ]
    if y_scaling:
        y_data = [
            [ y_axis*scale for y_axis,scale in zip(y_blob,scale_blob) ] for y_blob,scale_blob in zip(y_data,y_scaling)
        ]
    #dont do shit if not enough data
    if(len(x_data)<2):
        return

    #decide how many plots needed, prep plots
    plt_number = len(y_data) + plots_extra
    if(plt_number ==0):
        return
    rows = int(np.ceil(np.sqrt(plt_number)*1.2))
    cols = int(np.ceil(plt_number/rows))

    if main_title:
        plt.suptitle(main_title)
    else:
        plt.suptitle(f"{len(x_data)} datapoints")


    for i in range(plt_number):
        plt.subplot( rows, cols, i+1)
        if i < plt_number:
            if y_title:
                plt.title(y_title[i])
            if y_axis:
                plt.ylabel(y_axis[i])
            if x_axis:
                plt.xlabel(x_axis)
        else:
            plt.axis('off')  # Turn off axis for empty subplots
    keep_label_track = np.zeros(plt_number)
    handle_normal_plot(x_data,y_data,y_names,keep_label_track,rows,cols)
    handle_deriv_plot(x_data,data_arr,start_index  ,keep_label_track,derivative_settings, rows,cols)
    handle_extrema_periods(x_data,data_arr,start_index  ,extrema_settings,rows,cols)

    for i in range(plt_number):
        if keep_label_track[i]>0:
            plt.subplot(rows,cols,i+1)
            plt.legend()


    plt.tight_layout()
    if animate:
        plt.pause(delay)
        plt.clf()
    elif show_plot:
        plt.show()

    
def send_command(ser, command):
    command = f"{command}\n"
    ser.write(command.encode())  

def read_and_send(ser,command,title):
    def call_after(answer):
        send_command(ser,f"{command}{answer}")
    read_data(title,call_after)

active_thread = None
def read_data(title,call_after):
    global active_thread
    if active_thread:
        return
    def read_in():
        global active_thread
        print(f"start readin of {title} : ")
        suppress_output()
        answer = input("...")
        restore_output()
        call_after(answer)
        active_thread=None
    input_thread = threading.Thread(target=read_in)
    input_thread.daemon = True  
    input_thread.start()
    active_thread = input_thread
    

#main read function
def reset_read_serial(name,SERIAL_PORT,call_dict={},plot_all = 1,FOLDER = FOLDER_DEF,BAUD_RATE= BAUD_RATE_DEF , show_plot=False,
                x_index=None,x_axis=None,y_list=None,y_axis=None,plots_extra = 0,y_title=None,number_shown=1000, delay = 0.01,
                y_names=[],x_scaling=1,y_scaling=[],derivative_settings=[], extrema_settings=[] ):
    """
    resets serial then starts reading from serial into a csv file

    uses function :func:´plot_data´ to plot data when show_plot is used, in that case 
    x_index, x_axis,y_list,y_axis need to be set tho
    call_dict is supposed to be a dictionary containing functions which take one parameter

    if a keyboard key corresponding to a key in the dictionary is pressed, the corresponding function is called
    with the serial object as parameter
    
    """
    last_plot = 0
    loop = True
    global_val = [last_plot,loop]
    if show_plot:
        to_check = [x_index,x_axis,y_list, y_axis]
        for i,check in enumerate(to_check):
            if check==None:
                raise Exception(f"x_index, x_axis,y_list,y_axis need to be set when using show_plot=True, failed at check {i}")

    call_check_dict =  {key: False for key in call_dict}
    def keyboard_listener(ser,call_dict,call_check_dict,global_val):
        while global_val[1]:
            if keyboard.is_pressed('alt'):
                for key in call_dict:
                    if keyboard.is_pressed(key):
                        if not call_check_dict[key]:
                            call_dict[key](ser)
                            call_check_dict[key] = True
                    else:  
                        call_check_dict[key] = False
                if keyboard.is_pressed("q"):
                    print("ending reading via keyboard q press")
                    global_val[1] = False
            time.sleep(0.1)

    if not os.path.exists(FOLDER):
        try:
            os.makedirs(FOLDER)
        except OSError as e:
            print(f"Error creating folder: {e}")

    current_datetime = datetime.now()
    time_str = current_datetime.strftime("%Y-%m-%d--%H-%M-%S")
    csv_file_name = f"{FOLDER}/{name}-{time_str}.csv"
    # open serial connection
    ser = serial.Serial(SERIAL_PORT, BAUD_RATE)
    
    # reset board, flush input, start again
    ser.setDTR(False)
    ser.flushInput()
    ser.setDTR(True)
    keyboard_thread = threading.Thread(target=keyboard_listener, args=(ser,call_dict,call_check_dict,global_val))
    keyboard_thread.daemon = True  # Set it as daemon so it stops when the main program ends
    keyboard_thread.start()

    # Open CSV file for writing
    with open(csv_file_name, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
    
        data_arr = []
        while global_val[1]:
            curr_time = time.time()
            line = ser.readline().decode().strip()
            print(line)
            if line.startswith("--"):
                continue
            data = line.split(';')

            #for the start
            if len(data_arr) ==0:
                [data_arr.append([]) for e in data]

            num_data = min(len(data_arr),len(data))
            [data_arr[i].append(to_float(data[i])) for i in range(num_data)]

            csv_writer.writerow([line])

            if(show_plot and (curr_time-global_val[0])>plot_all):
                global_val[0] = curr_time
                try:
                    plot_data(data_arr,x_index,x_axis,y_list,y_axis,plots_extra = plots_extra,
                            y_title=y_title,number_shown=number_shown, delay = delay,
                            y_names=y_names,x_scaling=x_scaling,y_scaling=y_scaling,
                            derivative_settings=derivative_settings, extrema_settings=extrema_settings,animate=True  )
                except Exception as e:
                    pass

                
    # Close serial connection
    ser.close()
