from utils.numeric_math import *
from utils.symbol_math import *
import sympy as sp


zero_inK = 273.15    # zero degrees in kelvin
room_temp_inK = 298.15  # room temp 25 degrees in kelvin

T = sp.Symbol("T")
T_0 = sp.Symbol(f"T_{{0}}")
B = sp.Symbol(f"B")
a = sp.Symbol( f"a")

p = sp.Symbol( f"p")
R_t = sp.Symbol( f"R_{{t}}")
R_0 = sp.Symbol( f"R_{{0}}")

T_func_simple = B/(B/T_0 - sp.log(a-1)- sp.log(R_0) + sp.log(R_t))
B_func_simple = (sp.log(a-1) + sp.log(R_0) - sp.log(R_t)) / (1/T_0 -1/T)

T_func = B/(B/T_0 - sp.log(1023/p-1)- sp.log(R_0) + sp.log(R_t))
parameter_list_T = [T_0,R_0,R_t,B,p]

rel_uncert_T, error_symbols_T, T_uncert = gaussian_error_propagation(T_func,T,use_rel_errors=True,output_rel=True)
rel_uncert_T = partial_factoring(rel_uncert_T, setting= PartialFactoringSetting(use_exponent_weight=False,put_into_fraction = False))

B_func = (sp.log(1023/p-1) + sp.log(R_0) - sp.log(R_t)) / (1/T_0 -1/T)
parameter_list_B = [T_0,R_0,R_t,T,p]

rel_uncert_B, error_symbols_B, B_uncert = gaussian_error_propagation(B_func,B,use_rel_errors=True,output_rel=True)

rel_uncert_B = partial_factoring(rel_uncert_B, setting= PartialFactoringSetting(use_exponent_weight=False,put_into_fraction = False))

dict_np_func = { 
    T: get_np_func_uncert(parameter_list_T,T_func,rel_uncert_T,error_symbols_T),
    B: get_np_func_uncert(parameter_list_B,B_func,rel_uncert_B,error_symbols_B),
}

print("finished loading fields")


if __name__ == "__main__":
    print("latex for temperature function: ")
    print(sp.latex(T_func_simple))
    print("latex for B function: ")
    print(sp.latex(B_func_simple))
    print()
    print(f"latex for relative uncertainty of temperature, symbol: {T_uncert}")
    print(sp.latex(rel_uncert_T))
    print()
    print(f"latex for relative uncertainty of B value, symbol: {B_uncert}")
    print(sp.latex(rel_uncert_B))
