
from utils.csv_handling import *
from utils.numeric_math import *
from symbolic_calc import *

B_grove = -4275
B_bread = -5445.14
rel_B_bread = 577
data = get_data_fromsearchkey("task17")

time = data["time"]*1e-3
temp_readout = data["readout_bread"]
T_f, rel_T_f = dict_np_func[T]
input_T = [room_temp_inK,1e5,1e5,B_bread,temp_readout]
fit_dict = get_equil_value(time,T_f(*input_T)-zero_inK, get_fit_dict=True, fit_name="task17 heater temp")

print(fit_dict["result"].fit_report())

